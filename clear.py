from os import environ

import discord
from discord import Intents

from discord.ext import commands

intents = Intents.default()

bot = commands.Bot(command_prefix=".", intents=intents)

my_guild = discord.Object(id=environ["CLEAR_GUILD"])


@bot.event
async def on_ready():
    print("Logged in as")
    print(bot.user.name)
    print(bot.user.id)
    print("------")
    await bot.tree.sync(guild=my_guild)
    await bot.tree.sync()
    print("Commands cleared")


bot.run(environ["BOT_TOKEN"])
