from redis.asyncio import Redis
from typing import Optional
from os import environ

redis = Redis(
    host=environ.get("REDIS_HOST", "localhost"), port=environ.get("REDIS_PORT", 6379)
)


async def _get_str(key: str) -> Optional[str]:
    v: bytes = await redis.get(key)
    return str(v, "utf-8") if v else None


async def store_lastfm(discord_id: int, lfm_name: str):
    await redis.set(f"LFMNames:{discord_id}", lfm_name)


async def get_lastm(discord_id) -> Optional[str]:
    return await _get_str(f"LFMNames:{discord_id}")


async def cache_image(id: str, image: bytes):
    await redis.set(f"ImageCache:{id}", image)
    await redis.expire(f"ImageCache:{id}", 60 * 60 * 24 * 7)


async def get_cached_image(id: str) -> Optional[bytes]:
    return await redis.get(f"ImageCache:{id}")


async def delete_cached_image(id: str):
    await redis.delete(f"ImageCache:{id}")
