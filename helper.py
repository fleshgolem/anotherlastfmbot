import functools

from discord import Member, Message, Interaction
from discord.ext import commands

from discord.ext.commands import Context

from lastfm import LFMError
from storage import get_lastm
from typing import List, TypeVar, Optional, Tuple
import re


async def get_last_fm_for_interaction(interaction: Interaction) -> str:
    username = await get_lastm(interaction.user.id)
    if username:
        return username
    else:
        raise LFMError(LFMError.Type.NOT_SET)


def require_last_fm(func):
    @functools.wraps(func)
    async def wrapped(ctx: Context, *args, **kwargs):
        username = await get_lastm(get_user_for_context(ctx).id)
        if username:
            return await func(ctx, lfm_user=username, *args, **kwargs)
        else:
            await ctx.send("Last.fm username not set. Please use `/fmset <username>`")

    return wrapped


def get_user_for_context(ctx: commands.Context) -> Member:
    message: Message = ctx.message
    return message.author


T = TypeVar("T")


def chunks(lst: List[T], n: int) -> List[List[T]]:
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def translate_lfm_period(val: str) -> Optional[str]:
    return {
        "w": "7day",
        "1m": "1month",
        "3m": "3month",
        "6m": "6month",
        "y": "12month",
        "all": "overall",
    }.get(val)


_size_re = re.compile(r"(\d)x(\d)")


def parse_chart_size(val: str) -> Tuple[Optional[int], Optional[int]]:
    match = _size_re.match(val)
    if not match:
        return None, None
    return min(6, int(match.group(1))), min(6, int(match.group(2)))
