# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = lfm_top_albums_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Any, List
from enum import Enum
from .converters import from_str, from_list, to_class, to_enum
from .LFMTracks import Image, Size


@dataclass
class Artist:
    url: str
    name: str
    mbid: str

    @staticmethod
    def from_dict(obj: Any) -> "Artist":
        assert isinstance(obj, dict)
        url = from_str(obj.get("url"))
        name = from_str(obj.get("name"))
        mbid = from_str(obj.get("mbid"))
        return Artist(url, name, mbid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["url"] = from_str(self.url)
        result["name"] = from_str(self.name)
        result["mbid"] = from_str(self.mbid)
        return result


@dataclass
class AlbumAttr:
    rank: int

    @staticmethod
    def from_dict(obj: Any) -> "AlbumAttr":
        assert isinstance(obj, dict)
        rank = int(from_str(obj.get("rank")))
        return AlbumAttr(rank)

    def to_dict(self) -> dict:
        result: dict = {}
        result["rank"] = from_str(str(self.rank))
        return result


@dataclass
class Album:
    artist: Artist
    attr: AlbumAttr
    image: List[Image]
    playcount: int
    url: str
    name: str
    mbid: str

    @staticmethod
    def from_dict(obj: Any) -> "Album":
        assert isinstance(obj, dict)
        artist = Artist.from_dict(obj.get("artist"))
        attr = AlbumAttr.from_dict(obj.get("@attr"))
        image = from_list(Image.from_dict, obj.get("image"))
        playcount = int(from_str(obj.get("playcount")))
        url = from_str(obj.get("url"))
        name = from_str(obj.get("name"))
        mbid = from_str(obj.get("mbid"))
        return Album(artist, attr, image, playcount, url, name, mbid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["artist"] = to_class(Artist, self.artist)
        result["@attr"] = to_class(AlbumAttr, self.attr)
        result["image"] = from_list(lambda x: to_class(Image, x), self.image)
        result["playcount"] = from_str(str(self.playcount))
        result["url"] = from_str(self.url)
        result["name"] = from_str(self.name)
        result["mbid"] = from_str(self.mbid)
        return result


@dataclass
class TopalbumsAttr:
    page: int
    per_page: int
    user: str
    total: int
    total_pages: int

    @staticmethod
    def from_dict(obj: Any) -> "TopalbumsAttr":
        assert isinstance(obj, dict)
        page = int(from_str(obj.get("page")))
        per_page = int(from_str(obj.get("perPage")))
        user = from_str(obj.get("user"))
        total = int(from_str(obj.get("total")))
        total_pages = int(from_str(obj.get("totalPages")))
        return TopalbumsAttr(page, per_page, user, total, total_pages)

    def to_dict(self) -> dict:
        result: dict = {}
        result["page"] = from_str(str(self.page))
        result["perPage"] = from_str(str(self.per_page))
        result["user"] = from_str(self.user)
        result["total"] = from_str(str(self.total))
        result["totalPages"] = from_str(str(self.total_pages))
        return result


@dataclass
class Topalbums:
    album: List[Album]
    attr: TopalbumsAttr

    @staticmethod
    def from_dict(obj: Any) -> "Topalbums":
        assert isinstance(obj, dict)
        album = from_list(Album.from_dict, obj.get("album"))
        attr = TopalbumsAttr.from_dict(obj.get("@attr"))
        return Topalbums(album, attr)

    def to_dict(self) -> dict:
        result: dict = {}
        result["album"] = from_list(lambda x: to_class(Album, x), self.album)
        result["@attr"] = to_class(TopalbumsAttr, self.attr)
        return result


@dataclass
class LFMTopAlbums:
    topalbums: Topalbums

    @staticmethod
    def from_dict(obj: Any) -> "LFMTopAlbums":
        assert isinstance(obj, dict)
        topalbums = Topalbums.from_dict(obj.get("topalbums"))
        return LFMTopAlbums(topalbums)

    def to_dict(self) -> dict:
        result: dict = {}
        result["topalbums"] = to_class(Topalbums, self.topalbums)
        return result


def lfm_top_albums_from_dict(s: Any) -> LFMTopAlbums:
    return LFMTopAlbums.from_dict(s)


def lfm_top_albums_to_dict(x: LFMTopAlbums) -> Any:
    return to_class(LFMTopAlbums, x)
