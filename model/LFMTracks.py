from dataclasses import dataclass
from typing import Any, List, Optional
from enum import Enum

from model.converters import (
    from_str,
    from_int,
    from_list,
    to_class,
    to_enum,
    from_stringified_bool,
    from_union,
    from_none,
)


@dataclass
class RecenttracksAttr:
    page: int
    per_page: int
    user: str
    total: int
    total_pages: int

    @staticmethod
    def from_dict(obj: Any) -> "RecenttracksAttr":
        assert isinstance(obj, dict)
        page = int(from_str(obj.get("page")))
        per_page = int(from_str(obj.get("perPage")))
        user = from_str(obj.get("user"))
        total = int(from_str(obj.get("total")))
        total_pages = int(from_str(obj.get("totalPages")))
        return RecenttracksAttr(page, per_page, user, total, total_pages)

    def to_dict(self) -> dict:
        result: dict = {}
        result["page"] = from_str(str(self.page))
        result["perPage"] = from_str(str(self.per_page))
        result["user"] = from_str(self.user)
        result["total"] = from_str(str(self.total))
        result["totalPages"] = from_str(str(self.total_pages))
        return result


@dataclass
class Album:
    mbid: str
    text: str

    @staticmethod
    def from_dict(obj: Any) -> "Album":
        assert isinstance(obj, dict)
        mbid = from_str(obj.get("mbid"))
        text = from_str(obj.get("#text"))
        return Album(mbid, text)

    def to_dict(self) -> dict:
        result: dict = {}
        result["mbid"] = from_str(self.mbid)
        result["#text"] = from_str(self.text)
        return result


@dataclass
class TrackAttr:
    nowplaying: bool

    @staticmethod
    def from_dict(obj: Any) -> "TrackAttr":
        assert isinstance(obj, dict)
        nowplaying = from_stringified_bool(from_str(obj.get("nowplaying")))
        return TrackAttr(nowplaying)

    def to_dict(self) -> dict:
        result: dict = {}
        result["nowplaying"] = from_str(str(self.nowplaying).lower())
        return result


@dataclass
class Date:
    uts: int
    text: str

    @staticmethod
    def from_dict(obj: Any) -> "Date":
        assert isinstance(obj, dict)
        uts = int(from_str(obj.get("uts")))
        text = from_str(obj.get("#text"))
        return Date(uts, text)

    def to_dict(self) -> dict:
        result: dict = {}
        result["uts"] = from_str(str(self.uts))
        result["#text"] = from_str(self.text)
        return result


class Size(Enum):
    EXTRALARGE = "extralarge"
    LARGE = "large"
    MEDIUM = "medium"
    SMALL = "small"


@dataclass
class Image:
    size: Size
    text: str

    @staticmethod
    def from_dict(obj: Any) -> "Image":
        assert isinstance(obj, dict)
        size = Size(obj.get("size"))
        text = from_str(obj.get("#text"))
        return Image(size, text)

    def to_dict(self) -> dict:
        result: dict = {}
        result["size"] = to_enum(Size, self.size)
        result["#text"] = from_str(self.text)
        return result


@dataclass
class Track:
    artist: Album
    mbid: str
    album: Album
    streamable: int
    url: str
    name: str
    image: List[Image]
    attr: Optional[TrackAttr] = None
    date: Optional[Date] = None

    @staticmethod
    def from_dict(obj: Any) -> "Track":
        assert isinstance(obj, dict)
        artist = Album.from_dict(obj.get("artist"))
        mbid = from_str(obj.get("mbid"))
        album = Album.from_dict(obj.get("album"))
        streamable = int(from_str(obj.get("streamable")))
        url = from_str(obj.get("url"))
        name = from_str(obj.get("name"))
        image = from_list(Image.from_dict, obj.get("image"))
        attr = from_union([TrackAttr.from_dict, from_none], obj.get("@attr"))
        date = from_union([Date.from_dict, from_none], obj.get("date"))
        return Track(artist, mbid, album, streamable, url, name, image, attr, date)

    def to_dict(self) -> dict:
        result: dict = {}
        result["artist"] = to_class(Album, self.artist)
        result["mbid"] = from_str(self.mbid)
        result["album"] = to_class(Album, self.album)
        result["streamable"] = from_str(str(self.streamable))
        result["url"] = from_str(self.url)
        result["name"] = from_str(self.name)
        result["image"] = from_list(lambda x: to_class(Image, x), self.image)
        result["@attr"] = from_union(
            [lambda x: to_class(TrackAttr, x), from_none], self.attr
        )
        result["date"] = from_union([lambda x: to_class(Date, x), from_none], self.date)
        return result


@dataclass
class Recenttracks:
    attr: RecenttracksAttr
    track: List[Track]

    @staticmethod
    def from_dict(obj: Any) -> "Recenttracks":
        assert isinstance(obj, dict)
        attr = RecenttracksAttr.from_dict(obj.get("@attr"))
        track = from_list(Track.from_dict, obj.get("track"))
        return Recenttracks(attr, track)

    def to_dict(self) -> dict:
        result: dict = {}
        result["@attr"] = to_class(RecenttracksAttr, self.attr)
        result["track"] = from_list(lambda x: to_class(Track, x), self.track)
        return result


@dataclass
class LFMTracksResponse:
    recenttracks: Recenttracks

    @staticmethod
    def from_dict(obj: Any) -> "LFMTracksResponse":
        assert isinstance(obj, dict)
        recenttracks = Recenttracks.from_dict(obj.get("recenttracks"))
        return LFMTracksResponse(recenttracks)

    def to_dict(self) -> dict:
        result: dict = {}
        result["recenttracks"] = to_class(Recenttracks, self.recenttracks)
        return result


def lfm_tracks_response_from_dict(s: Any) -> LFMTracksResponse:
    return LFMTracksResponse.from_dict(s)


def lfm_tracks_response_to_dict(x: LFMTracksResponse) -> Any:
    return to_class(LFMTracksResponse, x)
