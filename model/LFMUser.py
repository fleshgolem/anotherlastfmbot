from dataclasses import dataclass
from typing import Any, List

from model.converters import from_str, from_int, from_list, to_class


@dataclass
class LFMImage:
    size: str
    text: str

    @staticmethod
    def from_dict(obj: Any) -> "LFMImage":
        assert isinstance(obj, dict)
        size = from_str(obj.get("size"))
        text = from_str(obj.get("#text"))
        return LFMImage(size, text)

    def to_dict(self) -> dict:
        result: dict = {}
        result["size"] = from_str(self.size)
        result["#text"] = from_str(self.text)
        return result


@dataclass
class LFMRegistered:
    unixtime: int
    text: int

    @staticmethod
    def from_dict(obj: Any) -> "LFMRegistered":
        assert isinstance(obj, dict)
        unixtime = int(from_str(obj.get("unixtime")))
        text = from_int(obj.get("#text"))
        return LFMRegistered(unixtime, text)

    def to_dict(self) -> dict:
        result: dict = {}
        result["unixtime"] = from_str(str(self.unixtime))
        result["#text"] = from_int(self.text)
        return result


@dataclass
class LFMUser:
    playlists: int
    playcount: int
    gender: str
    name: str
    subscriber: int
    url: str
    country: str
    image: List[LFMImage]
    registered: LFMRegistered
    type: str
    age: int
    bootstrap: int
    realname: str

    @staticmethod
    def from_dict(obj: Any) -> "LFMUser":
        assert isinstance(obj, dict)
        playlists = int(from_str(obj.get("playlists")))
        playcount = int(from_str(obj.get("playcount")))
        gender = from_str(obj.get("gender"))
        name = from_str(obj.get("name"))
        subscriber = int(from_str(obj.get("subscriber")))
        url = from_str(obj.get("url"))
        country = from_str(obj.get("country"))
        image = from_list(LFMImage.from_dict, obj.get("image"))
        registered = LFMRegistered.from_dict(obj.get("registered"))
        type = from_str(obj.get("type"))
        age = int(from_str(obj.get("age")))
        bootstrap = int(from_str(obj.get("bootstrap")))
        realname = from_str(obj.get("realname"))
        return LFMUser(
            playlists,
            playcount,
            gender,
            name,
            subscriber,
            url,
            country,
            image,
            registered,
            type,
            age,
            bootstrap,
            realname,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["playlists"] = from_str(str(self.playlists))
        result["playcount"] = from_str(str(self.playcount))
        result["gender"] = from_str(self.gender)
        result["name"] = from_str(self.name)
        result["subscriber"] = from_str(str(self.subscriber))
        result["url"] = from_str(self.url)
        result["country"] = from_str(self.country)
        result["image"] = from_list(lambda x: to_class(LFMImage, x), self.image)
        result["registered"] = to_class(LFMRegistered, self.registered)
        result["type"] = from_str(self.type)
        result["age"] = from_str(str(self.age))
        result["bootstrap"] = from_str(str(self.bootstrap))
        result["realname"] = from_str(self.realname)
        return result


@dataclass
class LFMUserResponse:
    user: LFMUser

    @staticmethod
    def from_dict(obj: Any) -> "LFMUserResponse":
        assert isinstance(obj, dict)
        user = LFMUser.from_dict(obj.get("user"))
        return LFMUserResponse(user)

    def to_dict(self) -> dict:
        result: dict = {}
        result["user"] = to_class(LFMUser, self.user)
        return result


def lfmuserresponse_from_dict(s: Any) -> LFMUserResponse:
    return LFMUserResponse.from_dict(s)


def lfmuserresponse_to_dict(x: LFMUserResponse) -> Any:
    return to_class(LFMUserResponse, x)
