from lastfm import get_top_albums, LFMError
from model.LFMTopAlbums import Album
from storage import cache_image, get_cached_image, delete_cached_image
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
from helper import chunks
from typing import List
from image_downloader import download_image_for
import asyncio


async def get_album_image_data(album: Album) -> bytes:
    url = album.image[2].text
    if not url:
        return open("assets/img/cancel.png", "rb").read()
    cached_image = await get_cached_image(url)
    if cached_image:
        return cached_image
    result = await download_image_for(
        album.artist.name, album.name, None, url, album.mbid
    )
    if not result.image_data:
        return open("assets/img/cancel.png", "rb").read()
    imgdata = result.image_data.read()
    await cache_image(url, imgdata)
    return imgdata


async def get_album_image(album: Album) -> Image.Image:
    data = await get_album_image_data(album)
    try:
        io = BytesIO(data)
        img = Image.open(io)
        return img
    except:
        url = album.image[2].text
        await delete_cached_image(url)
        data = await get_album_image_data(album)
        io = BytesIO(data)
        try:
            # Apparently this can still cause errors
            img = Image.open(io)
            return img
        except:
            return Image.open("assets/img/cancel.png")


async def get_all_album_image(albums: [Album]) -> [Image.Image]:
    coros = [get_album_image(a) for a in albums]
    return await asyncio.gather(*coros)


async def generate_chart(
    username: str, period: str, row_count: int, column_count: int
) -> BytesIO:
    albums = await get_top_albums(
        username, limit=row_count * column_count, period=period
    )
    if len(albums.album) == 0:
        raise LFMError(LFMError.Type.NO_PLAYS)
    images = await get_all_album_image(albums.album)

    album_names: List[str] = [f"{a.artist.name} - {a.name}" for a in albums.album]

    album_name_lengths = [len(a) for a in album_names]
    max_length = max(album_name_lengths)
    text_space = int(max_length * 7)

    padding = 20

    size = (
        column_count * 100 + padding * 3 + text_space,
        row_count * 100 + padding * 2,
    )
    canvas = Image.new("RGBA", size, (0, 0, 0, 255))
    draw_covers_on_canvas(canvas, images, padding, column_count)
    draw_names_on_canvas(canvas, album_names, padding, column_count)

    io = BytesIO()
    canvas.save(io, "PNG")
    io.seek(0)
    return io


def draw_names_on_canvas(canvas, album_names, padding, column_count):
    text_offset = column_count * 100 + padding * 2
    fnt = ImageFont.truetype("assets/fonts/arial-unicode-ms.ttf", 12)
    text_chunks = chunks(album_names, column_count)
    d = ImageDraw.Draw(canvas)
    for row_idx, row in enumerate(text_chunks):
        for col_idx, text in enumerate(row):
            d.text(
                (text_offset, row_idx * 100 + col_idx * 15 + padding),
                text,
                font=fnt,
                fill=(255, 255, 255, 255),
            )


def draw_covers_on_canvas(canvas, images, padding, count):
    image_chunks = chunks(images, count)
    for row_idx, row in enumerate(image_chunks):
        for col_idx, image in enumerate(row):
            target_size = (100, 100)
            resized = image.resize(target_size).convert("RGBA")
            canvas.paste(
                resized, (col_idx * 100 + padding, row_idx * 100 + padding), resized
            )


async def test():
    io = await generate_chart("nairbsteps", "7day", 5, 5)
    img = open("bla.png", "wb")
    img.write(io.read())


if __name__ == "__main__":
    import asyncio

    asyncio.run(test())
