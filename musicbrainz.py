from model.LFMTopAlbums import Album
from pydantic import BaseModel, Field
from typing import List, Optional
from httpx import AsyncClient
from lastfm import LFMError
from io import BytesIO


class MBRelease(BaseModel):
    id: str


class MBArtistCredit(BaseModel):
    name: str


class MBRecording(BaseModel):
    releases: List[MBRelease]
    artist_credit: MBArtistCredit = Field(alias="artist-credit")


class MBRecordingResult(BaseModel):
    recordings: List[MBRecording]


class MBReleaseGroup(BaseModel):
    releases: List[MBRelease]
    title: str
    artist_credit: MBArtistCredit = Field(alias="artist-credit")
    id: str


class MBReleaseGroupResult(BaseModel):
    release_groups: List[MBReleaseGroup] = Field(alias="release-groups")


_client = AsyncClient(
    headers={"User-Agent": "Shredditcord Bot", "Accept": "application/json"}
)


async def download_mb_image(
    artist: str, album: Optional[str], track: Optional[str], mbid: Optional[str]
) -> BytesIO:
    print(f"Using MB §{album} §{artist} §{mbid}")
    if mbid:
        url = f"http://coverartarchive.org/release/{mbid}/front"
    elif album:
        releasegroup = await find_mb_releasegroup(artist, album)
        url = f"http://coverartarchive.org/release-group/{releasegroup.id}/front"
    else:
        release = await find_mb_release(artist, track)
        url = f"http://coverartarchive.org/release/{release.id}/front"
    r = await _client.get(url, follow_redirects=True)
    if r.status_code != 200:
        raise LFMError(LFMError.Type.UNKNOWN_ERROR)
    return BytesIO(r.content)


async def find_mb_release(artist: str, title: str) -> MBRelease:
    url = "http://musicbrainz.org/ws/2/recording/"
    r = await _client.get(url, params={"query": f"artist:{artist},recording:{title}"})
    j = r.json()
    result = MBRecordingResult(**j)
    recording = [r for r in result.recordings if r.artist_credit.name == artist][0]

    return recording.releases[0]


async def find_mb_releasegroup(artist: str, album: str) -> MBReleaseGroup:
    url = "http://musicbrainz.org/ws/2/release-group/"
    r = await _client.get(
        url, params={"query": f"artist:{artist},releasegroup:{album}"}
    )
    j = r.json()
    result = MBReleaseGroupResult(**j)
    release_group = [
        r
        for r in result.release_groups
        if r.title == album and r.artist_credit.name == artist
    ][0]
    return release_group
