from lastfm import lfmrequest
from time import sleep
from datetime import datetime
import asyncio
import random

friends = [
    "It_all_adds_up",
    "Tobi_GER",
    "raiseer",
    "xGonzillax",
    "B1axun",
    "julzman",
    "mesharu",
    "mrsmia",
    "haenje",
    "WalronMedusus",
    "TWSammy",
    "wuyavae",
    "tt-bazille",
    "MastaOfGnarz",
    "macke2387",
    "MakaniChild",
    "zicke3k",
    "nicht-jens",
    "Nyradom",
    "conX",
    "PIAlisa",
    "desviste",
    "Apple-Of-Eve",
    "skka",
    "Sermer",
    "aenne2007",
    "diggen",
    "Maj033",
    "subraumspalt",
    "LivingBackwards",
    "chrizzlpooh",
    "ninetynine",
    "ief",
    "eeeee_Meister",
    "Beatschnitzel",
    "SirAbdul",
    "karmapolizei",
    "seppel_ska",
    "Crunge",
    "out0r",
    "Inti",
]


async def run():
    while True:
        print(datetime.now())
        response = await lfmrequest(
            "user.getrecenttracks", {"user": random.choice(friends), "limit": 50}
        )
        print(response.json())
        sleep(5)


asyncio.run(run())
