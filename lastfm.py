import httpx
from enum import Enum
from model.LFMUser import lfmuserresponse_from_dict, LFMUser
from model.LFMTracks import lfm_tracks_response_from_dict, Track
from model.LFMTopAlbums import Topalbums, lfm_top_albums_from_dict
from typing import Optional
from io import BytesIO
from os import environ
import asyncio


class LFMError(Exception):
    class Type(Enum):
        UNKNOWN_ERROR = 0
        USER_NOT_FOUND = 1
        NO_TRACK = 2
        NO_PLAYS = 3
        UNREACHABLE = 4
        NOT_SET = 5
        PRIVATE = 6

    def __init__(self, type: Type):
        self.type = type


RETRY_LIMIT = 3


async def lfmrequest(command: str, params=None, retrycount: int = 0):
    if params is None:
        params = {}
    params["method"] = command
    params["format"] = "json"
    params["api_key"] = environ["LASTFM_KEY"]
    try:
        async with httpx.AsyncClient() as client:
            r = await client.get(f"https://ws.audioscrobbler.com/2.0/", params=params)
            if r.status_code >= 400:
                if r.status_code == 403:
                    raise LFMError(LFMError.Type.PRIVATE)
                elif r.status_code == 500 and retrycount < RETRY_LIMIT:
                    await asyncio.sleep(1)
                    return await lfmrequest(command, params, retrycount=retrycount + 1)
                elif r.status_code >= 400:
                    print(f"LFM Error {r.status_code} - {r.text}")
                    raise LFMError(LFMError.Type.UNKNOWN_ERROR)
            return r
    except LFMError as e:
        raise e
    except Exception:
        if retrycount < RETRY_LIMIT:
            await asyncio.sleep(1)
            return await lfmrequest(command, params, retrycount=retrycount + 1)
        raise LFMError(LFMError.Type.UNREACHABLE)


async def check_user(username: str) -> LFMUser:
    response = await lfmrequest("user.getinfo", {"user": username})
    if response.status_code == 200:
        userresp = lfmuserresponse_from_dict(response.json())
        return userresp.user
    elif response.status_code == 404:
        raise LFMError(LFMError.Type.USER_NOT_FOUND)
    else:
        raise LFMError(LFMError.Type.UNKNOWN_ERROR)


async def get_last_track(username: str) -> Optional[Track]:
    response = await lfmrequest("user.getrecenttracks", {"user": username, "limit": 2})
    if response.status_code == 200:
        trackresp = lfm_tracks_response_from_dict(response.json())
        if len(trackresp.recenttracks.track) > 0:
            return trackresp.recenttracks.track[0]
        else:
            raise LFMError(LFMError.Type.NO_TRACK)
    else:
        raise LFMError(LFMError.Type.UNKNOWN_ERROR)


async def get_top_albums(
    username: str, limit: int = 25, period: str = "7day"
) -> Topalbums:
    response = await lfmrequest(
        "user.gettopalbums", {"user": username, "limit": limit, "period": period}
    )
    if response.status_code == 200:
        topresp = lfm_top_albums_from_dict(response.json())
        return topresp.topalbums
    else:
        raise LFMError(LFMError.Type.UNKNOWN_ERROR)


async def download_image(url: str) -> BytesIO:
    async with httpx.AsyncClient() as client:
        r = await client.get(url)
        if r.status_code != 200:
            raise LFMError(LFMError.Type.UNKNOWN_ERROR)
        return BytesIO(r.content)
