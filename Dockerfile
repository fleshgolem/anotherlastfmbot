FROM python:3.10
WORKDIR /app
RUN pip install poetry
COPY poetry.lock .
COPY pyproject.toml .
RUN poetry config virtualenvs.create false; poetry env info
RUN poetry install --no-root --no-dev
COPY . .
CMD ["python", "-u", "bot.py"]