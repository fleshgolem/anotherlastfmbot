from lastfm import download_image as download_lastfm_image
from musicbrainz import download_mb_image
from model.LFMTracks import Track
from dataclasses import dataclass
from io import BytesIO
from typing import Optional
from storage import cache_image, get_cached_image


@dataclass
class AlbumImageData:
    artist: str
    album: str
    image_data: Optional[BytesIO]


async def download_image(track: Track) -> AlbumImageData:
    image_url = track.image[3].text

    album = track.album
    album_name = None
    track_name = None
    mbid = None
    artist_name = track.artist.text
    if album and album.text:
        album_name = album.text
        mbid = album.mbid
    else:
        track_name = track.name
    cached_image = await get_cached_image(image_url)
    if cached_image:
        return AlbumImageData(
            artist_name, album_name or track_name or "", BytesIO(cached_image)
        )
    return await download_image_for(
        artist_name, album_name, track_name, image_url, mbid
    )


async def download_image_for(
    artist_name: str,
    album_name: Optional[str],
    track_name: Optional[str],
    image_url: str,
    mbid: str | None,
) -> AlbumImageData:
    # this seems to be last.fms default large cover, show an error in this case

    if (
        not image_url
        or image_url
        == "https://lastfm.freetls.fastly.net/i/u/300x300/2a96cbd8b46e442fc41c2b86b821562f.png"
    ):
        return await fallback_download_image(artist_name, album_name, track_name, mbid)
    try:
        image = await download_lastfm_image(image_url)
        await cache_image(image_url, image.read())
        image.seek(0)
        return AlbumImageData(artist_name, album_name or "", image)
    except Exception as firsterror:
        try:
            # try once again
            image = await download_lastfm_image(image_url)
            return AlbumImageData(artist_name, album_name or "", image)
        except Exception as seconderror:
            return await fallback_download_image(
                artist_name, album_name, track_name, mbid
            )


async def fallback_download_image(
    artist: str, album: Optional[str], track: Optional[str], mbid: Optional[str]
) -> AlbumImageData:
    try:
        image = await download_mb_image(artist, album, track, mbid)
        return AlbumImageData(artist, album or "", image)
    except:
        return AlbumImageData(artist, album or "", None)
