from enum import Enum
from os import environ
from typing import Optional
from uuid import uuid4

import discord
from discord import Member, Embed, app_commands, File, InteractionResponse, User
from discord.app_commands import Choice

import lastfm
from discord.ext import commands

from chart import generate_chart
from helper import get_last_fm_for_interaction
from image_downloader import download_image
from model.LFMUser import LFMUser
from storage import store_lastfm

my_guild = None
if guild_id := environ.get("TEST_GUILD"):
    my_guild = discord.Object(id=guild_id)


async def fm_command(discord_user: Member | User, lfm_user: str):
    track = await lastfm.get_last_track(lfm_user)
    user = await lastfm.check_user(lfm_user)
    embed = Embed()
    embed.type = "rich"
    embed.title = f"Current track for {lfm_user} ({discord_user.display_name})"
    embed.url = user.url
    if track:
        embed.set_thumbnail(url=track.image[2].text)
        embed.add_field(name="Track", value=track.name)
        embed.add_field(name="Artist", value=track.artist.text)
        if track.album and track.album.text:
            embed.add_field(name="Album", value=track.album.text)
    embed.set_footer(
        text=f"{user.name} has {user.playcount} scrobbles.",
        icon_url=discord_user.display_avatar.url,
    )
    return embed


async def fmset_command(discord_user: Member | User, username: str) -> LFMUser:
    user = await lastfm.check_user(username)
    await store_lastfm(discord_user.id, user.name)
    return user


async def handle_error(interaction: discord.Interaction, error: Exception):
    if isinstance(error, lastfm.LFMError):
        msg = {
            error.Type.USER_NOT_FOUND: "User not found",
            error.Type.NO_TRACK: "No tracks played",
            error.Type.UNKNOWN_ERROR: "An unknown error occured. Please try again later",
            error.Type.NO_PLAYS: "No plays on this account for the given period",
            error.Type.UNREACHABLE: ":headstone::headstone: Last.fm API is down :headstone::headstone:",
            error.Type.NOT_SET: "Last.fm username not set. Please use `/fmset username`",
        }[error.type]
        await interaction.followup.send(content=msg)
    else:
        await interaction.followup.send(content="Unknown error occured")


@app_commands.command(description="Show currently playing/last played track")
async def fm(interaction: discord.Interaction):
    try:
        await interaction.response.defer()
        lfm_user = await get_last_fm_for_interaction(interaction)
        embed = await fm_command(interaction.user, lfm_user)
        await interaction.followup.send(embed=embed)
    except Exception as e:
        await handle_error(interaction, e)


@app_commands.command(description="Connect with your last.fm account")
@app_commands.describe(lastfm_username="Your username on last.fm")
async def fmset(interaction: discord.Interaction, lastfm_username: str):
    try:
        await interaction.response.defer()
        user = await fmset_command(interaction.user, lastfm_username)
        await interaction.followup.send(
            content=f"Set last.fm username '{user.name}' for {interaction.user.display_name}"
        )
    except Exception as e:
        await handle_error(interaction, e)


@app_commands.command(description="Show cover for currently playing track")
async def fmcover(interaction: discord.Interaction):
    try:
        await interaction.response.defer()
        lfm_user = await get_last_fm_for_interaction(interaction)
        track = await lastfm.get_last_track(lfm_user)
        if not track:
            await interaction.followup.send(content="No play history")
            return
        result = await download_image(track)
        if not result.image_data:
            await interaction.followup.send(
                content=f"Could not download cover for `{result.artist} - {result.album}`"
            )
        else:
            await interaction.followup.send(
                content=f"Cover for `{result.artist} - {result.album}`",
                files=[File(result.image_data, filename=f"{uuid4()}.png")],
            )

    except Exception as e:
        await handle_error(interaction, e)


@app_commands.command(description="Generate last.fm chart")
@app_commands.choices(
    period=[
        Choice(name="this week", value="7day"),
        Choice(name="1 month", value="1month"),
        Choice(name="3 months", value="3month"),
        Choice(name="6 months", value="6month"),
        Choice(name="1 year", value="12month"),
        Choice(name="all time", value="overall"),
    ]
)
async def fmchart(
    interaction: discord.Interaction,
    period: Choice[str],
    width: Optional[app_commands.Range[int, 1, 6]],
    height: Optional[app_commands.Range[int, 1, 6]],
):
    try:
        await interaction.response.defer()
        lfm_user = await get_last_fm_for_interaction(interaction)
        chart = await generate_chart(lfm_user, period.value, height or 5, width or 5)
        await interaction.followup.send(
            content=f"Here is your chart for {period.name}:",
            files=[File(chart, filename=f"{uuid4()}.png")],
        )
    except Exception as e:
        await handle_error(interaction, e)


def setup_tree(bot: commands.Bot):
    # bot.add_cog(Commands(), guild=discord.Object(id=87090364627820544))
    bot.tree.add_command(fm, guild=my_guild)
    bot.tree.add_command(fmset, guild=my_guild)
    bot.tree.add_command(fmcover, guild=my_guild)
    bot.tree.add_command(fmchart, guild=my_guild)
