from os import environ
from typing import Optional

from discord import File, Intents, Object
from discord.ext import commands

import lastfm
from commands import fm_command, my_guild, setup_tree, fmset_command
from helper import (
    get_user_for_context,
    require_last_fm,
    translate_lfm_period,
    parse_chart_size,
)
from uuid import uuid4
from chart import generate_chart
from image_downloader import download_image
from dotenv import load_dotenv
import sentry_sdk

load_dotenv()

if sentry_dsn := environ.get("SENTRY_DSN"):
    sentry_sdk.init(
        dsn="https://f1c8914f5ec6f73314249f9c577d1f84@o4506424080007168.ingest.sentry.io/4506424274124800",
        environment=environ.get("SENTRY_ENV", "dev"),
    )

intents = Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix=".", intents=intents)


@bot.event
async def on_ready():
    if bot.user:
        print("Logged in as")
        print(bot.user.name)
        print(bot.user.id)
        print("------")
    await bot.tree.sync(guild=my_guild)
    if clear_guild_id := environ.get("CLEAR_GUILD"):
        clear_guild = Object(id=clear_guild_id)
        await bot.tree.sync(guild=clear_guild)


async def handle_error(ctx: commands.Context, error: lastfm.LFMError):
    msg = {
        error.Type.USER_NOT_FOUND: "User not found",
        error.Type.NO_TRACK: "No tracks played",
        error.Type.UNKNOWN_ERROR: "An unknown error occured. Please try again later",
        error.Type.NO_PLAYS: "No plays on this account for the given period",
        error.Type.UNREACHABLE: ":headstone::headstone: Last.fm is not reachable :headstone::headstone:",
        error.Type.PRIVATE: "Your listening history is set to private. There is just no way this can work",
    }[error.type]
    await ctx.send(msg)


@bot.command()
async def fmset(ctx: commands.Context, username: Optional[str] = None):
    """Set your username on last.fm"""
    if not username:
        await ctx.send("Please specify a username. `.fmset <username>`")
        return
    discord_user = get_user_for_context(ctx)

    try:
        user = await fmset_command(discord_user, username)
        await ctx.send(
            f"Set last.fm username '{user.name}' for {discord_user.display_name}"
        )
    except lastfm.LFMError as e:
        await handle_error(ctx, e)


@bot.command()
@require_last_fm
async def fm(ctx: commands.Context, **kwargs):
    """Show what you are currently playing"""
    lfm_user = kwargs["lfm_user"]
    discord_user = get_user_for_context(ctx)

    try:
        embed = await fm_command(discord_user, lfm_user)
        await ctx.send(embed=embed)
    except lastfm.LFMError as e:
        await handle_error(ctx, e)


@bot.command(aliases=["fmc"])
@require_last_fm
async def fmcover(ctx: commands.Context, **kwargs):
    """Show cover of album you are currently playing"""
    lfm_user = kwargs["lfm_user"]
    try:
        track = await lastfm.get_last_track(lfm_user)
        if not track:
            await ctx.send("No play history")
            return
        result = await download_image(track)
        if not result.image_data:
            await ctx.send(
                f"Could not download cover for `{result.artist} - {result.album}`"
            )
        else:
            await ctx.send(
                f"Cover for `{result.artist} - {result.album}`",
                file=File(result.image_data, filename=f"{uuid4()}.png"),
            )
    except lastfm.LFMError as e:
        await handle_error(ctx, e)


@bot.command()
@require_last_fm
async def fmchart(
    ctx: commands.Context, period: str | None = None, size: str = "5x5", **kwargs
):
    """Create a chart of your top albums

    Valid periods: 'w', '1m', '3m', '6m', 'y', 'all'
    Valid size: '1x1' up to '6x6', defaults to '5x5'
    """
    lfm_user = kwargs["lfm_user"]
    try:
        if not period:
            await ctx.send(
                """
**Usage: .fmchart <period> <size>**

Valid periods: 'w', '1m', '3m', '6m', 'y', 'all'
Valid size: '1x1' up to '6x6', defaults to '5x5'

Example: `.fmchart w 5x5`
"""
            )
            return
        lfm_period = translate_lfm_period(period)
        if not lfm_period:
            await ctx.send(
                f"Invalid time period '{period}'. Please use 'w', '1m', '3m', '6m', 'y' or 'all'"
            )
            return
        row, column = parse_chart_size(size)
        if not (row and column):
            await ctx.send(
                f"Invalid size format '{size}'. Please use 'MxN', e.g. '5x5'"
            )
            return
        try:
            chart = await generate_chart(lfm_user, lfm_period, row, column)
        except lastfm.LFMError as e:
            await handle_error(ctx, e)
            return
        user = get_user_for_context(ctx)
        await ctx.send(
            f"{user.mention} here is your chart:",
            file=File(chart, filename=f"{uuid4()}.png"),
        )
    except lastfm.LFMError as e:
        await handle_error(ctx, e)


setup_tree(bot)
bot.run(environ["BOT_TOKEN"])
